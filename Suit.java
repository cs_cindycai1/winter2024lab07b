public enum Suit {
	HEARTS(0.4),
	SPADES(0.3),
	DIAMONDS(0.2),
	CLUBS(0.1);
	
	//FIELDS
	private final double score;
	
	//CONSTRUCTIR
	private Suit(double score) {
		this.score = score;
	}
	
	//GET METHOD
	public double getScore() {
		return this.score;
	}
}