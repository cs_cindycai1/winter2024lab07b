public class Card {
	private Suit suit;
	private Rank rank;
	
	//constructor
	public Card(Suit suit, Rank rank) {
		this.suit = suit;
		this.rank = rank;
	}
	
	//get methods
	public Suit getSuit() {
		return this.suit;
	}
	
	public Rank getRank() {
		return this.rank;
	}
	
	//toString
	public String toString() {
		return this.rank + " Of " + this.suit;
	}
	
	//CUSTOM METHODS
	//returns the score of a card using its value and suit
	public double calculateScore() {
		return this.rank.getScore() + this.suit.getScore();
	}
}