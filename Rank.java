public enum Rank {
	ACE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	TEN(10),
	JACK(11),
	QUEEN(12),
	KING(13);
	
	//FIELDS
	private final double score;
	
	//CONSTRUCTOR
	private Rank(double score) {
		this.score = score;
	}
	
	//GET METHOD
	public double getScore() {
		return this.score;
	}
}