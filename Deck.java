import java.util.Random;

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//CONSTRUCTOR
	public Deck() {
		rng = new Random();
		numberOfCards = 52;
		
		this.cards = new Card[numberOfCards];
		//another way*
		int cardCounter = 0;
		while (cardCounter < this.numberOfCards) {
			for (Suit suit: Suit.values()) {
				for (Rank rank: Rank.values()) {
					this.cards[cardCounter] = new Card(suit, rank);
					cardCounter++;
				}
			}
		}
	}
	
	//GET METHODS
	public int getNumberOfCards() {
		return this.numberOfCards;
	}
	
	//TOSTRING
	public String toString() {
		String result = "";
		
		for (int i = 0; i < this.numberOfCards; i++) {
			result += "\nCard " + (i + 1) + ": " + this.cards[i];
		}
		
		return result;
	}
	
	//CUSTOM METHODS
	//return last card of the deck and subtract the number of card by 1
	public Card drawTopCard() {
		numberOfCards--;
		return this.cards[numberOfCards];
	}
	
	//shuffle the deck
	public void shuffle() {
		for (int i = 0; i < this.numberOfCards; i++) {
			int switchIndex = rng.nextInt(numberOfCards);
			Card temp = this.cards[i];
			this.cards[i] = this.cards[switchIndex];
			this.cards[switchIndex] = temp;
		}
	}
}