public class SimpleWar {
	public static void main(String[] args) {
		Deck pile = new Deck();
		pile.shuffle();
		
		int player1Point = 0;
		int player2Point = 0;
		
		int round = 1;
		
		//draw pair of cards and compare them until no card is left
		while (pile.getNumberOfCards() > 0) {
			System.out.println("Round " + round);
			round++;
			
			Card player1Card = pile.drawTopCard();
			Card player2Card = pile.drawTopCard();
			
			double card1Point = player1Card.calculateScore();
			double card2Point = player2Card.calculateScore();
			
			System.out.println("Player 1 has " + player1Card + ", worth " + card1Point + " points");
			System.out.println("Player 2 has " + player2Card + ", worth " + card2Point + " points");
			
			//compare card scores
			if (card1Point > card2Point) {
				System.out.println("\nPlayer 1 win this round!\n");
				player1Point++;
			} else {
				System.out.println("\nPlayer 2 win this round!\n");
				player2Point++;
			}
			
			System.out.println("Player 1's points:" + player1Point);
			System.out.println("Player 2's points:" + player2Point);
			
			System.out.println("*******************************");
		}
		
		//print winner of game
		if (player1Point > player2Point) {
			System.out.println("Congratulation, Player 1 won!");
		} else if (player1Point < player2Point) {
			System.out.println("Congratulation, Player 2 won!");
		} else {
			System.out.println("It's a tie!");
		}
	}
}